﻿using System;
using System.IO;
using Spectre.Console;

namespace SpikeV2
{
    class Program
    {
        static void Main(string[] args)
        {
            var choice = AnsiConsole.Prompt(
                new SelectionPrompt<string>()
                .Title("[cornflowerblue]Choose an Option[/]")
                .AddChoices(new[] {
                    "Write", "Read", "Delete"
                })
            );

            switch (choice) {
                case "Write":
                Write();
                return;
                case "Read":
                Read();
                return;
                case "Delete":
                Delete();
                return;
            }
        }

        private static void Write() {
            Console.Clear();
            var Name = AnsiConsole.Prompt(
                new TextPrompt<string>("[yellow]Enter a name for the file (Please also include a extension |e.g. .txt .js|)[/]")
                .PromptStyle("blue")
            );

            var name = Name.Trim();
   
            AnsiConsole.MarkupLine("[yellow]Type anything you want to append / write to this file...[/]");
            AnsiConsole.MarkupLine("[bold lime]To exit please type .exit()[/]");
            string text = Console.ReadLine();
            while (text != ".exit()") {
                File.AppendAllText(name, text + Environment.NewLine);
                text = Console.ReadLine();
            }
            AnsiConsole.MarkupLine("[bold green]Good Bye![/]");
        }

        private static void Read() {
            retry:
            Console.Clear();

            var Name = AnsiConsole.Prompt(
                new TextPrompt<string>("[yellow]Enter a name for the file[/]")
                .PromptStyle("blue")
            );
            var filename = Name.Trim();

            if (!File.Exists(filename)) {
                AnsiConsole.MarkupLine("[bold underline red]File does not exist[/]");
                AnsiConsole.MarkupLine("[bold yellow]Press any key to try again.[/]");
                Console.ReadKey(true);
                goto retry;
            }
            
            var fileRead = File.ReadAllText(filename);

            Console.Clear();
            AnsiConsole.MarkupLine("[mediumpurple4]Contents of: [/]" + filename);

            AnsiConsole.MarkupLine($"\n[cornflowerblue]{fileRead}[/]");


            AnsiConsole.MarkupLine("\n[yellow]Press any key to quit...[/]");
            Console.ReadKey(true);
        }
        private static void Delete() {
            retry:
            Console.Clear();

            var Name = AnsiConsole.Prompt(
                new TextPrompt<string>("[yellow]Enter a name for the file to delete[/]")
                .PromptStyle("blue")
            );
            var filename = Name.Trim();

            if (!File.Exists(filename)) {
                AnsiConsole.MarkupLine("[bold underline red]File does not exist[/]");
                AnsiConsole.MarkupLine("[bold yellow]Press any key to try again.[/]");
                Console.ReadKey(true);
                goto retry;
            }
            
            try {
                File.Delete(filename);
            }
            catch (System.IO.IOException e) {
                AnsiConsole.WriteException(e);
            }
            finally {
                Console.Clear();

                AnsiConsole.MarkupLine("\n[yellow]File deleted![/]");
                AnsiConsole.MarkupLine("\n[yellow]Press any key to quit...[/]");
                Console.ReadKey(true);
            }
        }   
    }
}
